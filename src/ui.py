from PySide2 import QtCore, QtQml
from asteval import Interpreter

from src.config import *
from src.utils import FUNC_EXPRESSIONS

import os


class UICalculator(QtCore.QObject):
    def __init__(self):
        super(UICalculator, self).__init__()

        self._engine = QtQml.QQmlApplicationEngine()
        self._engine.rootContext().setContextProperty(
            'APPLICATION_VERSION', APPLICATION_VERSION)

        self._engine.load(os.path.join(BASE_DIR, 'ui', 'App.qml'))
        self._window = self._engine.rootObjects()[0]

        self._connect()

        self._display = self._window.findChild(QtCore.QObject, 'tf_calc_in')

    def _connect(self):
        self.window.update_display.connect(self.update_display)
        self.window.solve.connect(self.solve)

    @property
    def window(self):
        return self._window

    @property
    def display(self):
        return self._display

    @QtCore.Slot(str)
    def update_display(self, symbol: str):
        content = self.display.property('text')

        if len(content) != 0 and symbol in FUNC_EXPRESSIONS.keys():
            self.display.setProperty(
                'text', FUNC_EXPRESSIONS[symbol].format(content))
        elif symbol not in FUNC_EXPRESSIONS.keys():
            self.display.setProperty('text', content + symbol)

    @QtCore.Slot()
    def solve(self):
        self.solve_definition()

    def solve_definition(self):
        content = self.display.property('text')
        if len(content) != 0:
            a_eval = Interpreter()
            res = str(a_eval(content))

            if len(a_eval.error) > 0:
                err, desc = a_eval.error[-1].get_error()

                if err == 'SyntaxError':
                    pass
                elif err == 'NotImplementedError':
                    pass
                elif err == 'NameError':
                    pass
            else:
                self.display.setProperty('text', res)
